from setuptools import setup, find_packages

setup(name='data_redis',
      version='0.0.1',
      description='DataDBS for Redis',
      url='http://www.gitlab.com/dpineda/data_redis',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=['data_redis'],
      install_requires=find_packages(),
      package_dir={'data_redis': 'data_redis'},
      package_data={
          'data_redis': ['../doc',
                        '../docs',
                        '../tests',
                        '../requeriments.txt']},
      zip_safe=False)
